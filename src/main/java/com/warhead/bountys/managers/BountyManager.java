/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main.java.com.warhead.bountys.managers;

import java.util.HashMap;
import main.java.com.warhead.bountys.Bounty;

/**
 *
 * @author mrodriguez
 */
public class BountyManager {
    
    private Bounty plugin;
    private static HashMap<String, Integer> placedBountys = new HashMap();
    
    public BountyManager(){
        plugin = Bounty.getInstance();
    }
    
    public boolean hasBounty(String playerName){
        if(placedBountys.containsKey(playerName)){
            return true;
        }
        return false;
    }
}

